# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 15:08:47 2015

@author: Sean
"""

import numpy as np
import matplotlib.pyplot as plt
import saverestore


def run():
    print 'Running Part I (Load in File Data)'
    time, vel = parti()
    print 'Running Part II (Plot Raw Doppler Data)'
    partii(time, vel)
    print 'Running Part III (Interpolate to Uniform Steps)'
    ntime, nvel = partiii(time, vel)
    print 'Running Part IV (Time Series Plotting)'
    partiv(time, vel, ntime, nvel)
    print 'Running Part VI (Trend Removal)'
    partvi(ntime, nvel)


def parti():
    """
    Loads in and adjusts the file data.
    """
    # Load in the file data
    time, vel = np.genfromtxt('../data/SUN_Velocity.txt', dtype='float64').T
    
    # Adjust and return the data
    time -= time[0]
    return time, vel


def partii(time, vel):
    """
    Plot the raw data.
    """
    # Scatter-plot the data
    plt.figure(1, figsize=(10, 7), dpi=200)
    plt.title('Raw File Data')
    plt.xlabel('Time (Days)')
    plt.ylabel('Doppler Velocity (m/s)')
    plt.xlim((-1, 31))
    plt.plot(time, vel)
    plt.savefig('../images/moss_project5Aii.png')


def partiii(time, vel):
    """
    Generates uniform step size data, using interpolation, returns the new data.
    """
    # Find the max step size in the original data
    diffs = time[1:] - time[:-1]
    maxstep = np.max(diffs)
    print '\tMax Diff is %f' % (maxstep)
    
    # Create the new uniform time step array
    nsteps = int(time[-1] / maxstep)
    print '\tStep count is %f:%d' % (time[-1], nsteps)
    ntime = np.arange(nsteps, dtype='float64')
    ntime *= maxstep
    
    # Interpolate the new data
    nvel = np.interp(ntime, time, vel)
    
    # Return the adjusted values
    return ntime, nvel


def partiv(time, vel, ntime, nvel):
    """
    Plot the different time series.
    """
    # Prepare the figure
    plt.figure(2, figsize=(10, 21), dpi=200)
    
    # Plot the entire time series
    plt.subplot(4, 1, 1)
    plt.title('Full Time Series')
    plt.xlabel('Time (Days)')
    plt.ylabel('Doppler Velocity (m/s)')
    plt.plot(time, vel, color='blue', label='Original')
    plt.plot(ntime, nvel, color='red', label='Interpolated')
    plt.xlim((-1, 31))
    plt.legend(loc='lower right')
    
    # Plot the 7 day period on the 11th
    index = np.where((time >= 11) & (time <= 18))
    nindex = np.where((ntime >= 11) & (ntime <= 18))
    plt.subplot(4, 1, 2)
    plt.title('7 Day Time Series')
    plt.xlabel('Time (Days)')
    plt.ylabel('Doppler Velocity (m/s)')
    plt.plot(time[index], vel[index], color='blue', label='Original')
    plt.plot(ntime[nindex], nvel[nindex], color='red', label='Interpolated')
    plt.xlim((10.8, 18.2))
    plt.legend(loc='lower right')
    
    # Plot the first 6 hours on the 11th
    index = np.where((time >= 11) & (time <= 11.25))
    nindex = np.where((ntime >= 11) & (ntime <= 11.25))
    plt.subplot(4, 1, 3)
    plt.title('6 Hour Time Series')
    plt.xlabel('Time (Days)')
    plt.ylabel('Doppler Velocity (m/s)')
    plt.plot(time[index], vel[index], color='blue', label='Original')
    plt.plot(ntime[nindex], nvel[nindex], color='red', label='Interpolated')
    plt.xlim((10.99, 11.26))
    plt.legend(loc='lower right')
    
    # Plot the first hour on the 11th
    index = np.where((time >= 11) & (time <= 11.0417))
    nindex = np.where((ntime >= 11) & (ntime <= 11.0417))
    plt.subplot(4, 1, 4)
    plt.title('First Hour Time Series')
    plt.xlabel('Time (Days)')
    plt.ylabel('Doppler Velocity (m/s)')
    plt.plot(time[index], vel[index], color='blue', label='Original')
    plt.plot(ntime[nindex], nvel[nindex], color='red', label='Interpolated')
    plt.xlim((10.995, 11.0467))
    plt.legend(loc='lower right')
    
    # Save the figure
    plt.tight_layout()
    plt.savefig('../images/moss_project5Aiv.png')
    

def partvi(ntime, nvel):
    """
    Remove trends and plot.
    """
    # Calculate the linear trend
    xlin = np.vstack((np.ones(len(ntime)), ntime))
    ylin = nvel
    step1 = np.linalg.inv(np.dot(xlin, xlin.T))
    step2 = np.dot(ylin, xlin.T)
    soln = np.dot(step1, step2)
    B, A = soln
    
    # Subtract the linear trend
    nvel -= (A * ntime + B)
    
    # Calculate the mean of the data and remove
    mean = np.mean(nvel)
    nvel -= mean
    
    # Prepare the figure
    plt.figure(3, figsize=(10, 14), dpi=200)
    
    # Plot the entire time series
    plt.subplot(2, 1, 1)
    plt.title('Full Adjusted Time Series')
    plt.xlabel('Time (Days)')
    plt.ylabel('Doppler Velocity (m/s)')
    plt.plot(ntime, nvel, color='blue')
    plt.xlim((-1, 31))
    
    # Save this data
    saverestore.save('../data/adjustedfull.dat', ('ntime', 'nvel'), (ntime, nvel))
    
    # Calculate smaller period
    nindex = np.where((ntime >= 11) & (ntime <= 18))
    ntime = np.array(ntime[nindex])
    nvel = np.array(nvel[nindex])
    
    # Calculate the linear trend
    xlin = np.vstack((np.ones(len(ntime)), ntime))
    ylin = nvel
    step1 = np.linalg.inv(np.dot(xlin, xlin.T))
    step2 = np.dot(ylin, xlin.T)
    soln = np.dot(step1, step2)
    B, A = soln
    
    # Subtract the linear trend
    nvel -= (A * ntime + B)
    
    # Calculate the mean of the data and remove
    mean = np.mean(nvel)
    nvel -= mean
    
    # Plot the 7 day period on the 11th
    plt.subplot(2, 1, 2)
    plt.title('7 Day Time Series')
    plt.xlabel('Time (Days)')
    plt.ylabel('Doppler Velocity (m/s)')
    plt.plot(ntime, nvel, color='blue')
    plt.xlim((10.8, 18.2))
    
    # Save this data
    saverestore.save('../data/adjustedpart.dat', ('ntime', 'nvel'), (ntime, nvel))
    
    # Save the figure
    plt.tight_layout()
    plt.savefig('../images/moss_project5Avi.png')