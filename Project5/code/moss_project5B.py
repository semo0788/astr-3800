# -*- coding: utf-8 -*-
"""
Created on Thu Nov 05 22:15:40 2015

@author: Sean
"""

import numpy as np
import matplotlib.pyplot as plt
import saverestore


def run():
    
    print 'Running Part I (Calculate Power Spectrum)'
    ffrq, fps, pfrq, pps = parti()
    print 'Running Part II (Plot Power Spectrum)'
    partii(ffrq, fps, pfrq, pps)
    print 'Running Part III (P-Mode Plotting)'
    partiii(ffrq, fps, pfrq, pps)
    print 'Running Part IV (Peak Spectrum Extraction and Plotting)'
    cfrq, cps = partiv(ffrq, fps)
    print 'Running Part VI (Chi-Squared Fitting)'
    partvi()
    
    
def parti():
    """
    Loads in the data and calculates the power spectra.
    Code taken from http://stackoverflow.com/questions/15382076.
    """
    # Load in the data
    fulltime, fullvel = saverestore.restore('../data/adjustedfull.dat')
    parttime, partvel = saverestore.restore('../data/adjustedpart.dat')
    
    # Calculate timestep
    tstep = fulltime[1] - fulltime[0]
    
    # Calculate the powerseries
    fullps = np.abs(np.fft.fft(fullvel))**2
    partps = np.abs(np.fft.fft(partvel))**2
    
    # Get the frequency arrays (in terms of cycles/day)
    fullfrq = np.fft.fftfreq(fullvel.size, tstep)
    partfrq = np.fft.fftfreq(partvel.size, tstep)
    
    # Normalize
    fullps /= len(fullps)
    partps /= len(partps)
    
    # Convert to cycles/sec then to mHz
    fullfrq /= 1e2
    partfrq /= 1e2
    
    # Return the frequencies and power spectra
    return fullfrq, fullps, partfrq, partps
    

def partii(ffrq, fps, pfrq, pps):
    """
    Plots the power spectra.
    """
    # Prepare the figure
    plt.figure(1, figsize=(10, 14), dpi=200)
    
    # Extract the positive frequencies
    findex = np.where(ffrq > 0.0)
    pindex = np.where(pfrq > 0.0)
    
    # Plot full power spectra
    plt.subplot(2, 1, 1)
    plt.title('Full Time Series Power Spectra')
    plt.loglog(ffrq[findex], fps[findex])
    plt.xlabel('Frequency (mHz)')
    plt.ylabel('Power Intensity')
    plt.xlim((1e-4, 10))
    plt.ylim((1e-5, 1e4))
    
    # Plot partial power spectra
    plt.subplot(2, 1, 2)
    plt.title('Partial Time Series Power Spectra')
    plt.loglog(pfrq[pindex], pps[pindex])
    plt.xlabel('Frequency (mHz)')
    plt.ylabel('Power Intensity')
    plt.xlim((1e-4, 10))
    plt.ylim((1e-5, 1e4))
    
    # Save the figure
    plt.tight_layout()
    plt.savefig('../images/moss_project5Bii.png')
    
    
def partiii(ffrq, fps, pfrq, pps):
    """
    Plots the p-mode spectra for each time period.
    """
    # Prepare the figure
    plt.figure(2, figsize=(10, 14), dpi=200)
    
    # Extract the p-mode frequencies
    findex = np.where((ffrq > 2.0) & (ffrq < 7.0))
    pindex = np.where((pfrq > 2.0) & (pfrq < 7.0))
    
    # Plot full power spectra
    plt.subplot(2, 1, 1)
    plt.title('Full Time Series P-Mode Power Spectra')
    plt.loglog(ffrq[findex], fps[findex])
    plt.xlabel('Frequency (mHz)')
    plt.ylabel('Power Intensity')
    plt.xlim((1.75, 6))
    plt.ylim((1e-4, 1e3))
    plt.xticks([2, 4, 6], [2, 4, 6])
    
    # Plot partial power spectra
    plt.subplot(2, 1, 2)
    plt.title('Partial Time Series P-Mode Power Spectra')
    plt.loglog(pfrq[pindex], pps[pindex])
    plt.xlabel('Frequency (mHz)')
    plt.ylabel('Power Intensity')
    plt.xlim((1.75, 6))
    plt.ylim((1e-4, 1e3))
    plt.xticks([2, 4, 6], [2, 4, 6])
    
    # Save the figure
    plt.tight_layout()
    plt.savefig('../images/moss_project5Biii.png')


def partiv(ffrq, fps):
    """
    Plot and extract the data around the power spectra peak of the sun.
    """
    # Extract the target frequencies
    lindex = np.where((ffrq > 3.2) & (ffrq < 3.4))
    cindex = np.where((ffrq > 3.25) & (ffrq < 3.35))
    
    # Prepare the figure
    plt.figure(3, figsize=(10, 7), dpi=200)
    
    # Plot the local series
    plt.title('Power Series Local to Solar Peak')
    plt.plot(ffrq[lindex], fps[lindex], label='3.2 mHz - 3.4 mHz')
    
    # Plot the close series
    plt.plot(ffrq[cindex], fps[cindex], label='3.25 mHz - 3.35 mHz', c='red')
    
    # Prettify the graph
    plt.xlabel('Frequency (mHz)')
    plt.ylabel('Power Intensity')    
    
    # Save the figure
    plt.tight_layout()
    plt.savefig('../images/moss_project5Biv.png')
    
    # Return the extracted data
    return ffrq[cindex], fps[cindex]


def partvi():
    """
    Calculate chi-squared.
    """
    pass