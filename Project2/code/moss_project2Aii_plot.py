# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 20:06:16 2015

@author: Sean
"""

import numpy as np
import matplotlib.pyplot as plt

def run():
    """
    Plots the star positions extracted from the image.
    """
    # Pull data from file
    dec, ra, mag = np.genfromtxt('../data/p2.csv', dtype='float', delimiter=',', skiprows=1).T
    
    # Calculate image information
    img_width = 2048
    x_scale = ((img_width / 2.0) * 0.396) / 3600.0
    img_ra = 354.473181333
    RAMIN = img_ra - x_scale
    RAMAX = img_ra + x_scale
    img_height = 1498
    y_scale = ((img_height / 2.0) * 0.396) / 3600.0
    img_dec = -0.929952240169
    DECMIN = img_dec - y_scale
    DECMAX = img_dec + y_scale
    
    # Create data for RA and DEC
    xlist = np.array(ra)
    ylist = np.array(dec)
    
    # Calculate transforms for magnitude
    mag = np.array(mag)
    magrange = np.max(mag) - np.min(mag)
    mag = mag - np.min(mag)
    mag = magrange - mag
    mag = (mag * 1.2) + 5 # A little boost to the range and minimum value to give it a good size
    
    # Create the axis data
    x = [0, img_width / 2, img_width]
    x2 = [round(RAMIN, 4), round(img_ra, 4), round(RAMAX, 4)]
    y = [0, img_height / 2, img_height]
    y2 = [round(DECMIN, 4), round(img_dec, 4), round(DECMAX, 4)]
    
    # Plot the data and set the plot information
    plt.scatter(xlist, ylist, s=mag)
    plt.title('Extracted Star Positions, with Magnitude')
    plt.axis([0, img_width, 0, img_height])
    plt.xlabel('Right Ascension (Degrees)')
    plt.ylabel('Declination (Degrees)')
    plt.xticks(x, x2)
    plt.yticks(y, y2)
    
    # Save the file
    plt.tight_layout()
    plt.savefig('../images/moss_project2Aii.png')
    