# -*- coding: utf-8 -*-
"""
Created on Tue Sep 15 15:30:58 2015

@author: Sean
"""

import saverestore
import numpy as np
import scipy.stats as stats


def extractCSV():
    """
    Extracts the ra, dec data from the csv file and returns it.
    """
    # Pull data from file
    dec, ra, null = np.genfromtxt('../data/p2.csv', dtype='float', delimiter=',', skiprows=1).T

    # Return the data    
    return (ra, dec)
    
    
def extractRandom():
    """
    Extracts the ra, dec data from the randomly generated data and returns it.
    """
    # Pull data from file
    (ra, dec) = saverestore.restore('../data/moss_project2Bi.dat')
    
    # Return the data
    return (ra, dec)
    

def run():
    """
    Extracts the star positions, compares the distance between them, and performs a chi-squared test
    between the histograms to see correlation to randomness.
    """
    # Known number of stars in the field
    NUM_STARS = 332
    
    # Get the data out of the files
    (cRA, cDEC) = extractCSV()
    (rRA, rDEC) = extractRandom()
    cPairs = np.array([(cRA[i], cDEC[i]) for i in range(NUM_STARS)])
    rPairs = np.array([(rRA[i], rDEC[i]) for i in range(NUM_STARS)])
    
    # Extract the nearest distance data for CSV data
    cData = []
    for star in range(NUM_STARS):
        tx, ty = cRA[star], cDEC[star]
        cData.append(min([(tx - cPairs[i][0])**2 + (ty - cPairs[i][1])**2 for i in range(NUM_STARS) if i != star]))
    cData = np.array(cData)
    
    # Extract the nearest distance data for random data
    rData = []
    for star in range(NUM_STARS):
        tx, ty = rRA[star], rDEC[star]
        rData.append(min([(tx - rPairs[i][0])**2 + (ty - rPairs[i][1])**2 for i in range(NUM_STARS) if i != star]))
    rData = np.array(rData)
    
    # Normalize the data, since it is in distance squared, and not distance
    cData = np.sqrt(cData)    
    rData = np.sqrt(rData)
    
    # Find the histogram limits
    histmax = max(np.max(cData), np.max(rData))
    
    # Generate the histogram data
    cHist,null = np.histogram(a=cData, bins=15, range=(0, histmax))
    rHist,null = np.histogram(a=rData, bins=15, range=(0, histmax))
    
    # Calculate the chi-squared value
    (chi, p) = stats.chisquare(f_obs=cHist, f_exp=rHist)
    
    # Print results
    print 'Chi squared value: (%f) P-Value: (%f)' % (chi, p)