# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 21:02:24 2015

@author: Sean
"""

import numpy as np
import saverestore
import matplotlib.pyplot as plt

def extractCSV():
    """
    Extracts the ra, dec data from the csv file and returns it.
    """
    # Pull data from file
    dec, ra, null = np.genfromtxt('../data/p2.csv', dtype='float', delimiter=',', skiprows=1).T

    # Return the data    
    return (ra, dec)
    
    
def extractRandom():
    """
    Extracts the ra, dec data from the randomly generated data and returns it.
    """
    # Pull data from file
    (ra, dec) = saverestore.restore('../data/moss_project2Bi.dat')
    
    # Return the data
    return (ra, dec)


def run():
    """
    Plots histogram data for nearest neighbor distances between extracted and generated data.
    """
    # The known number of stars
    NUM_STARS = 332
    
    # Get the data out of the files
    (cRA, cDEC) = extractCSV()
    (rRA, rDEC) = extractRandom()
    cPairs = np.array([(cRA[i], cDEC[i]) for i in range(NUM_STARS)])
    rPairs = np.array([(rRA[i], rDEC[i]) for i in range(NUM_STARS)])
    
    # Extract the nearest distance data for CSV data
    cData = []
    for star in range(NUM_STARS):
        tx, ty = cRA[star], cDEC[star]
        cData.append(min([(tx - cPairs[i][0])**2 + (ty - cPairs[i][1])**2 for i in range(NUM_STARS) if i != star]))
    cData = np.array(cData)
    
    # Extract the nearest distance data for random data
    rData = []
    for star in range(NUM_STARS):
        tx, ty = rRA[star], rDEC[star]
        rData.append(min([(tx - rPairs[i][0])**2 + (ty - rPairs[i][1])**2 for i in range(NUM_STARS) if i != star]))
    rData = np.array(rData)
    
    # Normalize the data, since it is in distance squared, and not distance
    cData = np.sqrt(cData)    
    rData = np.sqrt(rData)
    
    # Set up the figure info
    plt.figure(1)
    
    # Find histogram limits
    histmax = max(np.max(cData), np.max(rData))
    
    # Create the first histogram
    plt.subplot(2, 1, 1)
    plt.hist(cData, bins=15, facecolor='g', range=(0, histmax))
    plt.xlabel('Minumum Stellar Distances (Pixels)')
    plt.ylabel('Frequency')
    plt.title('Stellar Distance Distribution in Image Data')
    
    # Create the second histogram
    plt.subplot(2, 1, 2)
    plt.hist(rData, bins=15, facecolor='b', range=(0, histmax))
    plt.xlabel('Minumum Stellar Distances (Pixels)')
    plt.ylabel('Frequency')
    plt.title('Stellar Distance Distribution in Random Data')
    
    # Save the image
    plt.tight_layout()
    plt.savefig('../images/moss_project2Bii.png')