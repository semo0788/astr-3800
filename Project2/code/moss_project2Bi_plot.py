# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 20:56:05 2015

@author: Sean
"""

import numpy as np
import matplotlib.pyplot as plt
import saverestore

def run():
    """
    Plots the extracted star positions, based on magnitude.
    """
    # The known number of stars
    NUM_STARS = 332
    
    # Calculate image information
    img_width = 2048
    x_scale = ((img_width / 2.0) * 0.396) / 3600.0
    img_ra = 354.473181333
    RAMIN = img_ra - x_scale
    RAMAX = img_ra + x_scale
    img_height = 1498
    y_scale = ((img_height / 2.0) * 0.396) / 3600.0
    img_dec = -0.929952240169
    DECMIN = img_dec - y_scale
    DECMAX = img_dec + y_scale    
    
    # Pull out the ra, dec, and mag info
    ra = np.random.randint(0, img_width, size=NUM_STARS)
    dec = np.random.randint(0, img_height, size=NUM_STARS)
    
    # Save the data
    saverestore.save('../data/moss_project2Bi.dat', ('x', 'y'), (ra, dec))
    
    # Create the axis data
    x = [0, img_width / 2, img_width]
    x2 = [round(RAMIN, 4), round(img_ra, 4), round(RAMAX, 4)]
    y = [0, img_height / 2, img_height]
    y2 = [round(DECMIN, 4), round(img_dec, 4), round(DECMAX, 4)]
    
    # Plot the data and set the plot information
    plt.scatter(ra, dec)
    plt.title('Generated Star Positions')
    plt.axis([0, img_width, 0, img_height])
    plt.xlabel('Right Ascension (Degrees)')
    plt.ylabel('Declination (Degrees)')
    plt.xticks(x, x2)
    plt.yticks(y, y2)
    
    # Save the file
    plt.tight_layout()
    plt.savefig('../images/moss_project2Bi.png')