# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 15:07:05 2015

@author: Sean
"""

import numpy as np
from astropy.io import fits
import matplotlib.pyplot as plt

def run():
    """
    Simple displays the grayscale image, with some adjustments to remove negative values.
    """
    # Open the fits file and extract the data
    ffile = fits.open('../data/p2.fits')
    rawdata = ffile[0].data
    img_height = rawdata.shape[0]
    img_width = rawdata.shape[1]
    
    # Close the fits file
    ffile.close()
    
    # Remove the negative data and log the data￼
    logdata = rawdata - np.min(rawdata)
    logdata = np.log(logdata)
    
    # Calculate the RA and DEC data
    x_scale = ((img_width / 2.0) * 0.396) / 3600.0
    y_scale = ((img_height / 2.0) * 0.396) / 3600.0
    img_dec = -0.929952240169
    img_ra = 354.473181333
    DECMIN = img_dec - y_scale
    DECMAX = img_dec + y_scale
    RAMIN = img_ra - x_scale
    RAMAX = img_ra + x_scale
    
    # Create the axis data
    x = [0, img_width / 2, img_width]
    x2 = [round(RAMIN, 4), round(img_ra, 4), round(RAMAX, 4)]
    y = [0, img_height / 2, img_height]
    y2 = [round(DECMIN, 4), round(img_dec, 4), round(DECMAX, 4)]
    
    # Plot the image
    plt.imshow(logdata, origin='lower', cmap='gray')
    
    # Set the image data
    plt.title('Grayscale Image')
    plt.xlabel('Right Ascension (Degrees)')
    plt.ylabel('Declination (Degrees)')
    plt.xticks(x, x2)
    plt.yticks(y, y2)    
    
    # Save the image
    plt.tight_layout()
    plt.savefig('../images/moss_project2Ai.png')