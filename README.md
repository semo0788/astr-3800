# ASTR 3800 Code Repository

This contains the code for the Fall 2015 ASTR3800 class at the University of Colorado at Boulder. The author of the repository, and all code contained within, is Sean Moss (semo0788@colorado.edu), unless otherwise noted.
