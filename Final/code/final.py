# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 16:36:00 2015

@author: Sean
"""

import numpy as np
import astropy.io.fits as fits
import matplotlib.pyplot as plt
import scipy.optimize as spopt
import colorlookup


def run():
    
    print 'Loading in lines from fits and reference file.'
    reflines, wavelength, flux = load()
    print 'Plotting flux from fits file'
    plotObservedFlux(wavelength, flux)
    print 'Fitting data to a curve and flattening'
    flatflux = flattenFlux(wavelength, flux)
    print 'Plot the flattened data'
    plotFlattenedFlux(wavelength, flatflux)

    print 'Finding Redshift and Uncertainty in Redshift using different line shifts'
    redshifts = np.zeros(5)
    uncertainties = np.zeros(5)
    lens = np.zeros(5)
    # Calculate the uncertainty in wavelength (due to finite step size)
    wstep = np.average(wavelength[1:] - wavelength[:-1])
    for i in range(5):
        lens[i] = len(reflines)
        print '\tFinding redshift & uncertainty using %d lines.' % (lens[i])
        redshifts[i] = extractRedshift(reflines, wavelength, np.exp(np.abs(flatflux) * 0.02))
        uncertainties[i] = evaluateUncertainty(redshifts[i], reflines, wavelength, flux, wstep)
        reflines = reflines[:-1]
        
    print 'Graphing Uncertainty vs. Reference Lines'
    plotUncertainty(lens, uncertainties)
    

def load():
    """
    Loads in the data, both from the fits file and the line reference file. Only returns a small
    subset of the reference lines, that are most easily used.
    This subset (and indices) includes:
        * Halpha (18), NII (17, 19), SII (20, 21), ArIII (22), Hbeta (9), Hgamma (6), Hdelta (5), 
            OIII (7, 10, 11)
    Returns the reference lines, observed flux, and reference lines.
    """
    # Load in the reference lines
    reflines = np.genfromtxt('../data/linelist-0429-51820-0056.csv', delimiter=',', skiprows=1).T
    reflines = reflines[1]
    reflines = reflines[[ 5, 6, 7, 9, 10, 11, 17, 18, 19, 20, 21, 22 ]]
    
    # Load in the observed lines
    fitsdata = fits.getdata('../data/spec-0429-51820-0056.fits.gz')
    flux = np.array([ i[0] for i in fitsdata ])
    wavelength = np.array([ 10**i[1] for i in fitsdata ])
    
    # Return the data
    return reflines, wavelength, flux
    
    
def plotObservedFlux(wavelength, flux):
    """
    Plots the observed flux vs wavelength.
    """
    # Prepare the figure
    plt.figure(0, figsize=(9, 6))
    
    # Make it pretty
    plt.title('Blackbody Curve of Galaxy')
    plt.xlabel('Wavelength (Angstroms)')
    plt.ylabel('Relative Flux')
    
    # Plot the stuff
    plt.plot(wavelength, flux)
    
    # Prettify again and save
    plt.tight_layout()
    plt.savefig('../images/observed_flux.png')
    

def flattenFlux(wavelength, flux):
    """
    Fits the observed fluxes to a curve, and uses the curve to flatten the data.
    Returns the flattened data
    """
    # Define the fitting function
    def fitfunc(x, a, b, c, d, e, f):
        return a * (x**5) + b * (x**4) + c * (x**3) + d * (x**2) + e * x + f
        
    # Fit the curve and report
    popt, pcov = spopt.curve_fit(fitfunc, wavelength, flux)
    a, b, c, d, e, f = popt
    print '\tFitting values: a=%.5f, b=%.5f, c=%.5f, d=%.5f, e=%.5f, f=%.5f' % (a, b, c, d, e, f)
    
    # Flatten the data
    flatdat = flux - fitfunc(wavelength, a, b, c, d, e, f)
    
    # Return the flattened data
    return flatdat
    
    
def plotFlattenedFlux(wavelength, flux):
    """
    Plots the flattened flux vs wavelength.
    """
    # Prepare the figure
    plt.figure(1, figsize=(9, 6))
    
    # Make it pretty
    plt.title('Flattened Blackbody Curve')
    plt.xlabel('Wavelength (Angstroms)')
    plt.ylabel('Relative Flattened Flux')
    
    # Plot the stuff
    plt.plot(wavelength, np.exp(np.abs(flux) * 0.02))
    
    # Prettify again and save
    plt.tight_layout()
    plt.savefig('../images/flattened_flux.png')
    
    
def extractRedshift(reflines, wavelength, flux):
    """
    Extracts the lines from the flattened flux by looking for peaks. This will work by taking the
    known lines, and running them across the given data until it lines up the best.
    Returns the redshift.
    """
    # Define the wavelength interpolation function
    def wavelengthinterp(target):
        """
        Interpolates the target wavelength from the observed flux. Returns the interpolated flux.
        """
        interps = np.interp([target], wavelength, flux, left=0, right=0)
        return interps[0]
        
    # Define the correlation function
    def corellatelines(z):
        """
        Returns the corellation value for the provided redshift. Simply multiplies the shifted reference
        line by the same wavelength in the observed fluxes.
        """
        # Shift the lines
        shiftlines = reflines * (z + 1)
        # Get the interpolated fluxes
        interps = [ wavelengthinterp(i) for i in shiftlines ]
        # Return the sum of the multiples
        return np.sum(interps)
        
    # Run through z values 0-.5 by 1/100000th steps until it fits the best
    z = 0
    maxz = 0
    maxval = 0
    while z <= 0.25:
        val = corellatelines(z)
        if val > maxval:
            maxval = val
            maxz = z
        z += 0.00001
        
    # Report results
    print '\t\tRedshift (%d lines) is %.5f' % (len(reflines), maxz)
    
    # Return the results
    return maxz
    

def evaluateUncertainty(redshift, reflines, wavelength, flux, wstep):
    """
    Evaluates and reports the uncertainty value in the measurement of the redshift.
    """
    # Shifted lines due to redshift
    shiftlines = reflines * (redshift + 1)  
    
    # Calculate uncertainty in the lines
    lunc = ((shiftlines / reflines) - 1)
    lunc = np.sqrt(np.sum(lunc**2))
    lunc /= len(shiftlines)
    
    # Report and return the uncertainty
    print '\t\tUncertainty (%d lines) is %.5f.' % (len(reflines), lunc)
    return lunc
    
    
def plotUncertainty(numlines, uncertainties):
    """
    Plots the uncertainty of the measurement vs the number of reference lines used.
    """
    # Prepare the figure
    plt.figure(2, figsize=(9, 6))
    
    # Plot the data
    plt.scatter(numlines, uncertainties)
    
    # Prettify the graph
    plt.title('Uncertainties vs. Reference Lines')
    plt.xlabel('Number of Reference Lines')
    plt.ylabel('Uncertainty')
    
    # Define the fitting function
    def fitfunc(x, a, b):
        return a * x + b
        
    # Fit the curve and report
    popt, pcov = spopt.curve_fit(fitfunc, numlines, uncertainties)
    a, b = popt
    print '\tFitting values: a=%.5f, b=%.5f' % (a, b)
    
    # Plot the fitting function
    rmin = np.min(numlines)
    rmax = np.max(numlines)
    xvals = np.linspace(rmin, rmax, num=50, endpoint=True)
    plt.plot(xvals, fitfunc(xvals, a, b), color='red')
    plt.ylim(0.001, 0.0025)
    
    # Save the plot
    plt.savefig('../images/uncertainties.png')