# -*- coding: utf-8 -*-
"""
Created on Tue Sep 22 18:59:48 2015

@author: Sean
"""

import saverestore
import numpy as np
import matplotlib.pyplot as plot
from astropy.time import Time


def run():
    """
    Plots the ACGWIDTH and RMSA data over time.
    """
    # Load in the saved data    
    rDate, cDate, rWidth, cWidth, rRmsa, cRmsa = saverestore.restore('../data/moss_project3Bi.dat')
    
    # Generate the replacement axis data
    nx = [Time(dat, format='jd', out_subfmt='date_hm') for dat in rDate]
    nx = [dat.iso[dat.iso.find(' ')+1 :] for dat in nx]
    
    # Set up the figure
    plot.figure(1)
    
    # Plot AVGWIDTH data
    plot.subplot(1, 2, 1)
    plot.title('AVGWIDTH (R and K)')
    plot.xlabel('Time (Julian Date)')
    plot.ylabel('AVGWIDTH')
    plot.xticks(rDate, nx)
    plot.plot(rDate, rWidth, 'r', label='R Data')
    plot.plot(rDate, cWidth, 'b', label='K Data')
    plot.legend(bbox_to_anchor=(.5, .5), loc=10)
    
    # Plot RMSA data
    plot.subplot(1, 2, 2)
    plot.title('RMSA (R and K)')
    plot.xlabel('Time (Julian Date)')
    plot.ylabel('RMSA')
    plot.xticks(rDate, nx)
    plot.plot(rDate, rRmsa, 'r', rDate, cRmsa, 'b')
    
    # Layout and save the image
    plot.tight_layout()
    plot.savefig('../images/moss_project3Bii.png')