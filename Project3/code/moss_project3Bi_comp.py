# -*- coding: utf-8 -*-
"""
Created on Tue Sep 22 18:39:06 2015

@author: Sean
"""

import saverestore
from glob import glob
import numpy as np
from FitsFile import FitsFile


def run():
    """
    Extracts, organizes, sorts, and saves the required data out of the image headers.
    """
    # Detect the files with the given wildcards
    r_images = glob('*R.P.rdc*')
    c_images = glob('*K.P.rdc*')
    
    # Open the fits files
    r_images = [FitsFile(fname) for fname in r_images]
    c_images = [FitsFile(fname) for fname in c_images]
    
    # Sort by Julian Date, earliest to latest
    r_images.sort(key=lambda x: x['JULDATE'])
    c_images.sort(key=lambda x: x['JULDATE'])
    
    # Define and populate the arrays for the data
    rDate = np.array([ffile['JULDATE'] for ffile in r_images])
    cDate = np.array([ffile['JULDATE'] for ffile in c_images])
    rWidth = np.array([ffile['AVGWIDTH'] for ffile in r_images])
    cWidth = np.array([ffile['AVGWIDTH'] for ffile in c_images])
    rRmsa = np.array([ffile['RMSA'] for ffile in r_images])
    cRmsa = np.array([ffile['RMSA'] for ffile in c_images])
    
    # Save the data
    saverestore.save('../data/moss_project3Bi.dat', ('rd', 'cd', 'rw', 'cw', 'rr', 'cr'), (rDate, cDate, rWidth, cWidth, rRmsa, cRmsa))