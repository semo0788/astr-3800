# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 16:17:51 2015

@author: Sean
"""

from FitsFile import FitsFile
import matplotlib.pyplot as plot
import numpy as np


def run():
    """
    Loads and displays the image data and image slice flux data for the first images.
    """
    # Load in the fits files
    rdcFile = FitsFile('20050702.1702.HW.R.P.rdc.fits')
    contrastFile = FitsFile('20050702.1702.HW.R.P.contrast.fits')
    
    # Extract slice pixel data for rdc and contrast
    rRow = int(rdcFile.height / 2)
    cRow = int(contrastFile.height / 2)
    rSliceData = rdcFile.data[rRow]
    cSliceData = contrastFile.data[cRow]
    
    # Prepare the figure
    plot.figure(1, dpi=96, figsize=(6, 4))
    
    # Plot the rdc grayscale
    plot.subplot(2, 2, 1)
    plot.title('RDC Gray Scale')
    rdcFile.displayImage()
    
    # Plot the contrast image
    plot.subplot(2, 2, 2)
    plot.title('Contrast Gray Scale')
    contrastFile.displayImage()
    
    # Plot the rdc slice
    plot.subplot(2, 2, 3)
    plot.title('RDC Slice')
    plot.xlabel('Pixel Space')
    plot.ylabel('Image Intensity')
    plot.xlim(xmax=len(rSliceData))
    plot.xticks([0, 1024, 2048])
    plot.plot(rSliceData)
    
    # Plot the contrast slice
    plot.subplot(2, 2, 4)
    plot.title('Contrast Slice')
    plot.xlabel('Pixel Space')
    plot.ylabel('Image Intensity')
    plot.xlim(xmax=len(cSliceData))
    plot.xticks([0, 1024, 2048])
    plot.plot(cSliceData)
    
    # Save the figure
    plot.tight_layout()
    plot.savefig('../images/moss_project3Ai.png')