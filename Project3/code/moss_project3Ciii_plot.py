# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 17:35:39 2015

@author: Sean
"""
import numpy as np
from glob import glob
import matplotlib.pyplot as plot
import saverestore
from FitsFile import FitsFile


def run():
    """
    Plots the mean vs. avgwidth and variance vs. avgwidth over time for both filters.
    """
    # Get a list of the contrast files
    rFiles = glob('*R.P.contrast.fits')
    kFiles = glob('*K.P.contrast.fits')
    
    # Load the AVGWIDTH information from part Bi
    null, null, rWidth, kWidth, null, null = saverestore.restore('../data/moss_project3Bi.dat')
    
    # Sort the files based on the time included in the file name
    rFiles.sort(key=lambda x: int(x.split('.')[1]))
    kFiles.sort(key=lambda x: int(x.split('.')[1]))
    
    # Create files out of the file names
    rFiles = [FitsFile(fname) for fname in rFiles]
    kFiles = [FitsFile(fname) for fname in kFiles]
    
    # Flatten the data
    rFlat = [ffile.data[int(ffile.height / 2)] for ffile in rFiles]
    kFlat = [ffile.data[int(ffile.height / 2)] for ffile in kFiles]
    
    # Calculate the first moment of the data (mean)
    rMnt1 = [np.mean(dat) for dat in rFlat]
    kMnt1 = [np.mean(dat) for dat in kFlat]
    
    # Calculcate the second moment of the data (variance = stddev ** 2)
    rMnt2 = [np.std(dat)**2 for dat in rFlat]
    kMnt2 = [np.std(dat)**2 for dat in kFlat]
    
    # Prepare the figure
    plot.figure(1, figsize=(8, 6))
    
    # Graph the data for red continuum mean
    plot.subplot(2, 2, 1)
    plot.title('Red Continuum Mean vs. AVGWIDTH')
    plot.xlabel('AVGWIDTH')
    plot.ylabel('Mean')
    plot.scatter(rWidth, rMnt1)
    
    # Graph the data for red continuum variance
    plot.subplot(2, 2, 2)
    plot.title('Red Continuum Variance vs. AVGWIDTH')
    plot.xlabel('AVGWIDTH')
    plot.ylabel('Standard Deviation')
    plot.scatter(rWidth, rMnt2)
    
    # Graph the data for calcium mean
    plot.subplot(2, 2, 3)
    plot.title('Calcium Mean vs. AVGWIDTH')
    plot.xlabel('AVGWIDTH')
    plot.ylabel('Mean')
    plot.scatter(kWidth, kMnt1)
    
    # Graph the data for calcium variance
    plot.subplot(2, 2, 4)
    plot.title('Calcium Variance vs. AVGWIDTH')
    plot.xlabel('AVGWIDTH')
    plot.ylabel('Standard Deviation')
    plot.scatter(kWidth, kMnt2)
    
    # Layout and save the figure
    plot.tight_layout()
    plot.savefig('../images/moss_project3Ciii.png')