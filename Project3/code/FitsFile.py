# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 16:08:28 2015

@author: Sean
"""

from astropy.io import fits
import matplotlib.pyplot as plot


class FitsFile:
    """
    Represents a loaded fits file
    """
    
    @property
    def header(self):
        return self._header
        
    @property
    def data(self):
        return self._data
        
    @property
    def width(self):
        return self._data.shape[1]
        
    @property
    def height(self):
        return self._data.shape[0]
        
    @property
    def filename(self):
        return self._filename
        
    
    def __init__(self, filename):
        """
        Constructor, loads in the data and header from the file.
        """
        self._filename = filename;
        self._data, self._header = fits.getdata(filename, header=True)
    
    
    def __getitem__(self, key):
        """
        Allows array accessor use with strings.
        """
        if isinstance(key, str):
            return self._header[key]
        return None

    
    def displayImage(self, gray=True):
        """
        Displays the image with the settings specified outside of the function call.
        """
        graystr = None
        if gray:
            graystr = 'gray'
        
        plot.imshow(self._data, origin='lower', cmap=graystr)
