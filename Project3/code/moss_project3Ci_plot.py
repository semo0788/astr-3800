# -*- coding: utf-8 -*-
"""
Created on Wed Sep 23 15:51:45 2015

@author: Sean
"""

from FitsFile import FitsFile
from glob import glob
import numpy as np
import matplotlib.pyplot as plot


def run():
    """
    Extracts and plots the intensity PDFs for the images over time as histograms.
    """
    # Load in the file names
    rFiles = glob('*R.P.contrast.fits')
    kFiles = glob('*K.P.contrast.fits')
    
    # Sort the files based on the time included in the file name
    rFiles.sort(key=lambda x: int(x.split('.')[1]))
    kFiles.sort(key=lambda x: int(x.split('.')[1]))
    
    # Delete the FitsFiles that are no longer needed
    del rFiles[3:], kFiles[3:]
    
    # Create files out of the file names
    rFiles = [FitsFile(fname) for fname in rFiles]
    kFiles = [FitsFile(fname) for fname in kFiles]
    
    # Prepare the data by flattening and cutting
    rData = [rFile.data[int(rFile.height / 2)] for rFile in rFiles]
    kData = [kFile.data[int(kFile.height / 2)] for kFile in kFiles]
    rData = [dat[np.where(dat > -0.7)] for dat in rData]
    kData = [dat[np.where(dat > -0.7)] for dat in kData]
    
    # Extract the range data for the data
    rMax = [np.max(dat) for dat in rData]
    kMax = [np.max(dat) for dat in kData]
    rMax = max(rMax)
    kMax = max(kMax)
    
    # Prepare the figure
    plot.figure(1, figsize=(8, 5))
    
    # Plot the red image data
    plot.subplot(2, 1, 1)
    plot.title('Red Image PDFs')
    plot.hist(rData[0], range=(-0.7, rMax), bins=50, log=True, color='red', \
        label=rFiles[0].filename.split('.')[1], normed=True)
    plot.hist(rData[1], range=(-0.7, rMax), bins=50, log=True, color='orange', \
        label=rFiles[1].filename.split('.')[1], normed=True)
    plot.hist(rData[2], range=(-0.7, rMax), bins=50, log=True, color='yellow', \
        label=rFiles[2].filename.split('.')[1], normed=True)
    plot.legend(title='Local Times', bbox_to_anchor=(.25, .75), loc=10)
    
    # Plot the Ca II K image data
    plot.subplot(2, 1, 2)
    plot.title('Ca II K Image PDFs')
    plot.hist(kData[0], range=(-0.7, kMax), bins=50, log=True, color='red', \
        label=kFiles[0].filename.split('.')[1], normed=True)
    plot.hist(kData[1], range=(-0.7, kMax), bins=50, log=True, color='orange', \
        label=kFiles[1].filename.split('.')[1], normed=True)
    plot.hist(kData[2], range=(-0.7, kMax), bins=50, log=True, color='yellow', \
        label=kFiles[2].filename.split('.')[1], normed=True)
    plot.legend(title='Local Times', bbox_to_anchor=(.25, .75), loc=10)
    
    # Layout and save the plot
    plot.tight_layout()
    plot.savefig('../images/moss_project3Ci.png')