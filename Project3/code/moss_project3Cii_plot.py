# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 15:44:56 2015

@author: Sean
"""

import numpy as np
import matplotlib.pyplot as plot
from FitsFile import FitsFile
from glob import glob


def run():
    """
    Extracts and plots the Mean and Variance of each filter over time.
    """
    # Load in the file names
    rFiles = glob('*R.P.contrast.fits')
    kFiles = glob('*K.P.contrast.fits')
    
    # Sort the files based on the time included in the file name
    rFiles.sort(key=lambda x: int(x.split('.')[1]))
    kFiles.sort(key=lambda x: int(x.split('.')[1]))
    
    # Create files out of the file names
    rFiles = [FitsFile(fname) for fname in rFiles]
    kFiles = [FitsFile(fname) for fname in kFiles]
    
    # Flatten the data
    rFlat = [ffile.data[int(ffile.height / 2)] for ffile in rFiles]
    kFlat = [ffile.data[int(ffile.height / 2)] for ffile in kFiles]
    
    # Calculate the first moment of the data (mean)
    rMnt1 = [np.mean(dat) for dat in rFlat]
    kMnt1 = [np.mean(dat) for dat in kFlat]
    
    # Calculcate the second moment of the data (variance = stddev ** 2)
    rMnt2 = [np.std(dat)**2 for dat in rFlat]
    kMnt2 = [np.std(dat)**2 for dat in kFlat]
    
    # Calculate the replacement data for the x-axis
    xlbl = np.arange(len(rFiles))
    nx = [int(ffile.filename.split('.')[1]) for ffile in rFiles]
    
    # Prepare the figure
    plot.figure(1, figsize=(8, 6))
    
    # Graph the first moment for the red continuum files
    plot.subplot(2, 2, 1)
    plot.title('Mean for Red Continuum')
    plot.xlabel('Image Time')
    plot.plot(rMnt1)
    plot.xticks(xlbl, nx)
    
    # Graph the second moment for the red continuum files
    plot.subplot(2, 2, 2)
    plot.title('Variance for Red Continuum')
    plot.xlabel('Image Time')
    plot.plot(rMnt2)
    plot.xticks(xlbl, nx)
    
    # Graph the first moment for the calcium files
    plot.subplot(2, 2, 3)
    plot.title('Mean for Calcium')
    plot.xlabel('Image Time')
    plot.plot(kMnt1)
    plot.xticks(xlbl, nx)
    
    # Graph the second moment for the calcium files
    plot.subplot(2, 2, 4)
    plot.title('Standard Deviation for Calcium')
    plot.xlabel('Image Time')
    plot.plot(kMnt2)
    plot.xticks(xlbl, nx)
    
    # Layout and save the image
    plot.tight_layout()
    plot.savefig('../images/moss_project3Cii.png')