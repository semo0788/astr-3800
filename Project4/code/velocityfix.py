# -*- coding: utf-8 -*-
"""
Created on Sun Oct 11 18:24:10 2015

@author: Sean
"""

import numpy as np


def velocityfix(vels):
    """
    This function adjusts the given velocities to the new velocities.
    """
    # Speed of light in km/s
    c = 299792.458
    
    # Get the velocities devided by c
    voc = vels / c
    
    # Get the components of the wavelength ratio equation and calculate it
    top = np.sqrt(1 + voc)
    bottom = np.sqrt(1 - voc)
    wratio = top / bottom
    
    # Return the ratios times c
    return wratio * c