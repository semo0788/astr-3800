# -*- coding: utf-8 -*-
"""
Created on Mon Oct 05 22:24:27 2015

@author: Sean
"""

import numpy as np
import matplotlib.pyplot as plot
from chisquared import fitting_chisquare
from velocityfix import velocityfix


def run():
    
    # This is basically part 1, even though there is not an explicit function for it
    print 'Running (part i) loading datapoints for galaxy data'
    odist, ovel = loadOriginalCSV()
    ndist, nvel = loadNearCSV()
    fdist, fvel = loadFarCSV()
    
    # Run the rest of the parts
    print 'Running (part iii) Plotting adjusted values'
    ovel, nvel, fvel = partiii(odist, ovel, ndist, nvel, fdist, fvel)
    print 'Running (part iv) Plotting best fit lines'
    Ao, Bo, An, Bn, Af, Bf = partiv(odist, ovel, ndist, nvel, fdist, fvel)
    print 'Running (part v) chi-squared calculations'
    partv(odist, ovel, ndist, nvel, fdist, fvel, Ao, Bo, An, Bn, Af, Bf)
    
    # Save the results
    plot.tight_layout()
    plot.savefig('../images/moss_project4B.png')
    
    
def partiii(odist, ovel, ndist, nvel, fdist, fvel):
    """
    Adjusts the data and plots it.
    """
    # Fix the velocities
    oveln = velocityfix(ovel)
    nveln = velocityfix(nvel)
    fveln = velocityfix(fvel)
    
    # Prepare the plot figure
    plot.figure(1, figsize=(10, 8))
    
    # Generate the replacement axis values
    maxx = np.max(fdist)
    maxy = np.max(fveln)
    origx = [0, maxx / 2, maxx]
    newx = ['0', '%d Mpc' % (int(maxx / 2)), '%d Mpc' % (int(maxx))]
    newy = [0, maxy / 2, maxy]
    
    # Plot the data
    osc = plot.scatter(odist, oveln, color='b', marker='o')
    nsc = plot.scatter(ndist, nveln, color='g', marker='^')
    fsc = plot.scatter(fdist, fveln, color='r', marker='*')
    plot.title('Extragalactic Recessional Velocity vs. Distance with Best Fit Lines')
    plot.xticks(origx, newx)
    plot.yticks(newy)
    plot.xlabel('Distance')
    plot.ylabel('Recessional Velocity (km/s)')
    
    # Generate the first legend
    plot.legend((osc, nsc, fsc), ('Original', 'Near (<.125c)', 'Far (>.125c)'), bbox_to_anchor=(0.98, 0.2),\
        loc='upper right', title='Data Points')
        
    # Return the adjusted data
    return oveln, nveln, fveln


def partiv(odist, ovel, ndist, nvel, fdist, fvel):
    """
    Plots best fit lines.
    """
    # Set up for the linear regression for original data
    x = np.vstack((np.ones(len(odist)), odist))
    y = ovel
    
    # Do matrix linear regression steps for original data
    step1 = np.linalg.inv(np.dot(x, x.T))
    step2 = np.dot(y, x.T)
    soln = np.dot(step1, step2)
    
    # Extract the linear fit line data for original data
    Bo = soln[0]
    Ao = soln[1]
    
    # Set up for the linear regression for near data
    x = np.vstack((np.ones(len(ndist)), ndist))
    y = nvel
    
    # Do matrix linear regression steps for near data
    step1 = np.linalg.inv(np.dot(x, x.T))
    step2 = np.dot(y, x.T)
    soln = np.dot(step1, step2)
    
    # Extract the linear fit line data for near data
    Bn = soln[0]
    An = soln[1]
    
    # Set up for the linear regression for far data
    x = np.vstack((np.ones(len(fdist)), fdist))
    y = fvel
    
    # Do matrix linear regression steps for far data
    step1 = np.linalg.inv(np.dot(x, x.T))
    step2 = np.dot(y, x.T)
    soln = np.dot(step1, step2)
    
    # Extract the linear fit line data for far data
    Bf = soln[0]
    Af = soln[1]
    
    # Report the answers
    print '\tOriginal fit line is y = %dx + %d' % (Ao, Bo)
    print '\tNear fit line is y = %dx + %d' % (An, Bn)
    print '\tFar fit line is y = %dx + %d' % (Af, Bf)
    
    # Prepare the plotting data (The ranges are fiddled with until they look good on the graph)
    xovals = np.arange(0, 4500)
    yovals = Ao * xovals + Bo
    xnvals = np.arange(0, 15000)
    ynvals = An * xnvals + Bn
    xfvals = np.arange(0, 15000)
    yfvals = Af * xfvals + Bf
    
    # Overplot the best fit data
    plot.plot(xovals, yovals, color='b')
    plot.plot(xnvals, ynvals, color='g')
    plot.plot(xfvals, yfvals, color='r')
    
    # Return the regression fitting values for use in part iv
    return Ao, Bo, An, Bn, Af, Bf


def partv(odist, ovel, ndist, nvel, fdist, fvel, Ao, Bo, An, Bn, Af, Bf):
    """
    Calculates and displays the chi-squared values.
    """
    # Calculate the various chi-squared values and probability to exceed
    chio, po = fitting_chisquare(odist, ovel, Ao, Bo)
    chin, pn = fitting_chisquare(ndist, nvel, An, Bn)
    chif, pf = fitting_chisquare(fdist, fvel, Af, Bf)
    
    # Report the results
    print '\tHubble data: Chi-Squared = %f, PtE = %f' % (chio, po)
    print '\tNear data: Chi-Squared = %f, PtE = %f' % (chin, pn)
    print '\tFar data: Chi-Squared = %f, PtE = %f' % (chif, pf)


def loadOriginalCSV():
    """
    Loads in the original data, and returns the distance and velocities.
    """    
    # Load the data and transpose it as columns
    filedata = np.genfromtxt('../data/hubbleoriginal.csv', dtype='float', delimiter=',', missing_values=0.0).T
    return filedata[1][1:], filedata[2][1:]

    
def loadNearCSV():
    """
    Loads in the data for <1/8c from ned1dlevel5.csv, returns the distances and velocities.
    """
    # Load the data and transpose it as columns
    filedata = np.genfromtxt('../data/ned1dlevel5.csv', dtype='float', delimiter=',', missing_values=0.0).T
    return filedata[3][2:], filedata[11][2:]
    

def loadFarCSV():
    """
    Loads in the data for >1/8c from ned4dlevel5.csv, returns the distances and velocities.
    """
    # Load the data and transpose it as columns
    filedata = np.genfromtxt('../data/ned4dlevel5.csv', dtype='float', delimiter=',', missing_values=0.0).T
    return filedata[3][2:], filedata[10][2:]