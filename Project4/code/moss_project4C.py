# -*- coding: utf-8 -*-
"""
Created on Wed Oct 07 15:26:32 2015

@author: Sean
"""

import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plot
from chisquared import fitting_chisquare


def run():
    
    print 'Running (part 1) Load RA and DEC from hubble data'
    dist, vel, ra, dec = loadOriginalCSV()
    print 'Running (part 2) Convert to radians'
    ra, dec = partii(ra, dec)
    print 'Running (part 3) Data fitting'
    Ho, X, Y, Z = partiii(dist, vel, ra, dec)
    print 'Running (part 4) Plotting corrected data'
    avel, A, B = partiv(dist, vel, ra, dec, Ho, X, Y, Z)
    print 'Running (part 5) Compute chi-squared'
    partv(dist, avel, A, B)
    
    # Save the results
    plot.tight_layout()
    plot.savefig('../images/moss_project4C.png')
    
def partii(ra, dec):
    """
    Turns the ra and dec given in hours and degrees into radians.
    """
    # Easy conversion for the DEC
    decrad = dec * 3.14159265 / 180.0
    # Convert hours into degrees
    rarad = ra * 15.0
    # Convert degrees into radians
    rarad = rarad * 3.14159265 / 180.0
    
    return rarad, decrad
    
    
def partiii(dist, vel, ra, dec):
    """
    Fits the data to Hubble's adjusted equation, returns the values
    """
    # Calculate the adjusted values
    cc = np.cos(ra) * np.cos(dec)
    sc = np.sin(ra) * np.cos(dec)
    s = np.sin(dec)
    
    # Define the fitting function
    def fitfunc(x, Ho, X, Y, Z):
        return Ho * x + X * cc + Y * sc + Z * s
        
    # Fit the curve
    popt, pcov = curve_fit(fitfunc, dist, vel)
    
    # Report the results
    print '\tReported fitted values: Ho = %f, X = %f, Y = %f, Z = %f' % (popt[0], popt[1], popt[2], popt[3])
    
    # Return the fitted values
    return popt[0], popt[1], popt[2], popt[3]


def partiv(dist, vel, ra, dec, Ho, X, Y, Z):
    """
    Calculates a linear fit and plots the adjusted data. Returns the corrected data and linear fit values.
    """
    # Calculate the cos and sin values for the adjusted data
    cc = np.cos(ra) * np.cos(dec)
    sc = np.sin(ra) * np.cos(dec)
    s = np.sin(dec)
    
    # Adjust the data
    avel = Ho * dist - X * cc - Y * sc - Z * s
    
    # Setup the linear regression fitting
    x = np.vstack((np.ones(len(dist)), dist))
    y = avel
    
    # Fit the data to a linear plot
    step1 = np.linalg.inv(np.dot(x, x.T))
    step2 = np.dot(y, x.T)
    soln = np.dot(step1, step2)
    B = soln[0]
    A = soln[1]
    
    # Generate replacement values for the axis
    origx = [0, 1, 2]
    newx = ['0', '1 Mpc', '2 Mpc']
    newy = [0, 500, 1000]
    
    # Prepare the plot
    plot.figure(1, figsize=(8, 5))
    
    # Scatter plot the data
    plot.scatter(dist, avel)
    plot.title('Adjusted Extragalactic Recessional Velocity vs. Distance')
    plot.xticks(origx, newx)
    plot.yticks(newy)
    plot.xlabel('Distance')
    plot.ylabel('Recessional Velocity (km/s)')
    
    # Prepare the fitted data
    xvals = np.arange(0, np.max(dist) * 1.1)
    yvals = A * xvals + B
    
    # Plot the fitted line
    plot.plot(xvals, yvals, label='Best Fit')
    
    # Report the results
    print '\tLinear fit line is y = %fx + %f' % (A, B)
    
    # Return the values for use in partv
    return avel, A, B


def partv(dist, avel, A, B):
    """
    Calculates the chi-square value and probability to exceed.
    """
    # Calculate the values
    chi, p = fitting_chisquare(dist, avel, A, B)
    
    # Report the values
    print '\tChi-Squared value: %f' % (chi)
    print '\tProbability to exceed: %f' % (p)
    

def loadOriginalCSV():
    """
    Loads in the original data, and returns the distance, velocities, RAs, and DECs.
    """    
    # Load the data and transpose it as columns
    filedata = np.genfromtxt('../data/hubbleoriginal.csv', dtype='float', delimiter=',', missing_values=0.0).T
    return filedata[1][1:], filedata[2][1:], filedata[3][1:], filedata[4][1:]