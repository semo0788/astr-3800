# -*- coding: utf-8 -*-
"""
Created on Wed Sep 30 16:08:50 2015

@author: Sean
"""

import numpy as np
import matplotlib.pyplot as plot
from scipy import optimize
from scipy import stats
from chisquared import fitting_chisquare


def run():
    
    # Load in data  
    dist, vel = loadOriginalCSV()
    
    # Run the parts of the code
    print 'Running (Part i) Scatter Plot of Original Data'
    parti(dist, vel)
    print 'Running (Part ii) Least Squares Fitting'
    A, B = partii(dist, vel)
    print 'Running (Part iii) Overplot Confidence Interval'
    partiii(dist, vel)
    print 'Running (Parv iv) Calculate the Chi-Squared'
    partiv(dist, vel, A, B)
    
    # Save the results
    plot.legend(title='Lines', bbox_to_anchor=(0.02, 0.98), loc=2)
    plot.tight_layout()
    plot.savefig('../images/moss_project4A.png')
    

def parti(dist, vel):
    """
    Plots the raw distance vs velocity data.
    """
    # Prepare the plot figure
    plot.figure(1, figsize=(8, 5))
    
    # Generate replacement values
    origx = [0, 1, 2]
    newx = ['0', '1 Mpc', '2 Mpc']
    newy = [0, 500, 1000]
    
    # Plot the data
    plot.scatter(dist, vel)
    plot.title('Extragalactic Recessional Velocity vs. Distance')
    plot.xticks(origx, newx)
    plot.yticks(newy)
    plot.xlabel('Distance')
    plot.ylabel('Recessional Velocity (km/s)')


def partii(dist, vel):
    """
    Calculates the linear best fit line, then plots the fitted data.
    """
    # Set up for the linear regression
    x = np.vstack((np.ones(len(dist)), dist))
    y = vel
    
    # Do matrix linear regression steps
    step1 = np.linalg.inv(np.dot(x, x.T))
    step2 = np.dot(y, x.T)
    soln = np.dot(step1, step2)
    
    # Extract the linear fit line data
    B = soln[0]
    A = soln[1]
    print '\tLinear fit line is y = %fx + %f' % (A, B)
    
    # Prepare the plotting data
    xvals = np.arange(0, np.max(dist) * 1.1)
    yvals = A * xvals + B
    
    # Overplot the best fit data
    plot.plot(xvals, yvals, color='r', label='Best Fit')
    
    # Return the A and B values for use in part iv
    return A, B
    

def partiii(dist, vel):
    """
    Using code taken from http://stackoverflow.com/questions/24633664
    """
    # This will be used to generate the line functions
    def make_linear(x, a, b):
        return a * x + b
        
    # Find the information on the standard deviation count
    targci = 0.68
    nstd = stats.norm.ppf((1.0 + targci) / 2.0)
    
    # Calculate the non-adjusted fit and its standard deviation
    popt, pcov = optimize.curve_fit(make_linear, dist, vel)
    perr = np.sqrt(np.diag(pcov))
    
    # Calculate the values for the adjusted lines
    upperci = popt + (nstd * perr)
    lowerci = popt - (nstd * perr)
    
    # Report the modified lines
    print '\tUpper Confidence line is y = %fx + %f' % (upperci[0], upperci[1])
    print '\tLower Confidence line is y = %fx + %f' % (lowerci[0], lowerci[1])
    
    # Plot the interval lines
    plot.plot(dist, make_linear(dist, *upperci), color='g', linestyle='--', label='Upper 68% Confidence Interval')
    plot.plot(dist, make_linear(dist, *lowerci), color='y', linestyle='--', label='Lower 68% Confidence Interval')


def partiv(dist, vel, A, B):
    """
    Fits the data using the custom chi-squared function.
    """
    # Calculate the chi-squared value
    chi, p = fitting_chisquare(dist, vel, A, B)
    
    # Report the results
    print '\tChi-Squared value: %f' % (chi)
    print '\tProbability to exceed: %f' % (p)


def loadOriginalCSV():
    """
    Loads in the original data, and returns the distance and velocities.
    """    
    # Load the data and transpose it as columns
    filedata = np.genfromtxt('../data/hubbleoriginal.csv', dtype='float', delimiter=',', missing_values=0.0).T
    return filedata[1][1:], filedata[2][1:]