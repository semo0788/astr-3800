# -*- coding: utf-8 -*-
"""
Created on Wed Oct 07 15:37:37 2015

@author: Sean
"""

import numpy as np
from scipy.stats import chi2


def fitting_chisquare(xvals, yvals, A, B):
    """
    Custom chi-squared function for finding fitting to a best fit curve, with the given parameters.
    """
    # Calculate the adjusted array
    adj_array = (yvals - A * xvals - B)**2
    
    # Calculate the sigma for the y values
    sigma_y = np.sum(adj_array)
    sigma_y /= (len(xvals) - 2)
    sigma_y = np.sqrt(sigma_y)
    
    # Calculate the chi squared value
    chisq = np.sum(adj_array / (sigma_y**2))
    
    # Calculate the pvalue
    p = 1 - chi2.cdf(chisq, len(xvals) - 2)
    return chisq, p