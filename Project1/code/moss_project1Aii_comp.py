# -*- coding: utf-8 -*-
"""
Created on Tue Sep 08 20:31:43 2015

@author: Sean
"""

import numpy as np
import saverestore
import random

# Function that contains the code needed for this part of the homework
def action():
    
    # Define a list to save the lists in
    results = []
    
    # For each of the 3 lists
    for j in range(3):
        # Create the empty list
        results.append([])
        # Use a new seed (arbitrary range for the new seed)
        np.random.seed(random.randint(0, 1000000))
        # Generate 1000 numbers for the list
        for i in range(1000):
            results[j].append(np.random.uniform())
        
    # Save all three lists to the file
    saverestore.save('../data/moss_project1Aii.dat', ('a', 'b', 'c'), (np.array(results[0]), np.array(results[1]), np.array(results[2])))