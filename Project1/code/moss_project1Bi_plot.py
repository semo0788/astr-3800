# -*- coding: utf-8 -*-
"""
Created on Tue Sep 08 21:16:31 2015

@author: Sean
"""

import numpy as np
import matplotlib.pyplot as plt
import saverestore

# Function with the code for this part
def action():
    
    # Call the two parts
    plotPartI()
    plotPartII()


# Plotting the user-generated values
def plotPartI():
    
    # Load in data
    a = saverestore.restore('../data/moss_project1Ai.dat')[0]
    b = saverestore.restore('../data/pratt_project1Ai.dat')[0]
    c = saverestore.restore('../data/strabala_project1Ai.dat')[0]
    
    # Generate x-values
    x_values = np.arange(1000)
    
    # Define the figure information
    plt.figure(1, figsize=(12, 18))
    plt.subplots_adjust(hspace=.5)
    
    # Figure for moss data
    plt.subplot(6, 1, 1)
    plt.plot(x_values, a)
    plt.gca().axes.get_xaxis().set_ticks([])
    plt.ylabel('Values')
    plt.title('Moss Values Ai')
    
    # Figure for pratt data
    plt.subplot(6, 1, 2)
    plt.plot(x_values, b)
    plt.gca().axes.get_xaxis().set_ticks([])
    plt.ylabel('Values')
    plt.title('Pratt Values Ai')
    
    # Figure for strabala data
    plt.subplot(6, 1, 3)    
    plt.plot(x_values, c)
    plt.gca().axes.get_xaxis().set_ticks([])
    plt.ylabel('Values')
    plt.title('Strabala Values Ai')


# Plotting the numpy generated values
def plotPartII():
    
    # Load in info
    (a, b, c) = saverestore.restore('../data/moss_project1Aii.dat')
    
    # Generate x-values
    x_values = np.arange(1000)
    
    # Figure for 1st numpy data
    plt.subplot(6, 1, 4)
    plt.plot(x_values, a, 'g-')
    plt.gca().axes.get_xaxis().set_ticks([])
    plt.ylabel('Values')
    plt.title('Numpy Random Values 1')
    
    # Figure for 2nd numpy data
    plt.subplot(6, 1, 5)
    plt.plot(x_values, b, 'g-')
    plt.gca().axes.get_xaxis().set_ticks([])
    plt.ylabel('Values')
    plt.title('Numpy Random Values 1')
    
    # Figure for 3rd numpy data
    plt.subplot(6, 1, 6)    
    plt.plot(x_values, c, 'g-')
    plt.gca().axes.get_xaxis().set_ticks([])
    plt.ylabel('Values')
    plt.title('Numpy Random Values 1')
    
    # Save the image
    plt.savefig('../images/moss_project1Bi.png')