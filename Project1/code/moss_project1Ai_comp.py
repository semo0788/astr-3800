"""
Created on Tue Sep  8 18:04:21 2015

@author: Sean
"""

import numpy as np
import random
import saverestore

# Function that contains the code needed for this part of the homework
def action():
    
    # Define a list
    results = []
    
    # Generate 100 integers between 0 and 63
    for i in range(1000):
        num = random.randint(0, 63)
        results.append(num / 64.0)
    
    # Save the data as "a"
    saverestore.save('../data/moss_project1Ai.dat', ('a'), [np.array(results)])
