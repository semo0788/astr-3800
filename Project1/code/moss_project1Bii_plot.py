# -*- coding: utf-8 -*-
"""
Created on Tue Sep 08 22:57:13 2015

@author: Sean
"""

import numpy as np
import matplotlib.pyplot as plt
import saverestore

# Contains the code for this part
def action():
    
    # Load in the data from the files
    a = saverestore.restore('../data/moss_project1Ai.dat')[0]
    b = saverestore.restore('../data/pratt_project1Ai.dat')[0]
    c = saverestore.restore('../data/strabala_project1Ai.dat')[0]
    (d, e, f) = saverestore.restore('../data/moss_project1Aii.dat')
    
    # Set up graph settings
    plt.figure(1, figsize=(16, 20))
    plt.subplots_adjust(hspace=1)#, vspace=.2)
    
    # Generate for individual human runs, bin size 0.1
    plt.subplot(12, 2, 1)
    plt.hist(a, bins=10, facecolor='g', range=(0, 1), align='mid')
    plt.ylabel('Count in Bins')
    plt.xlabel('Bin Value')
    plt.title('Moss Data (Bin Size = 0.1)')
    plt.subplot(12, 2, 3)
    plt.hist(b, bins=10, facecolor='g', range=(0, 1), align='mid')
    plt.ylabel('Count in Bins')
    plt.xlabel('Bin Value')
    plt.title('Pratt Data (Bin Size = 0.1)')
    plt.subplot(12, 2, 5)
    plt.hist(c, bins=10, facecolor='g', range=(0, 1), align='mid')
    plt.ylabel('Count in Bins')
    plt.xlabel('Bin Value')
    plt.title('Strabala Data (Bin Size = 0.1)')
    
    # Generate for individual numpy runs, bin size 0.1
    plt.subplot(12, 2, 7)
    plt.hist(d, bins=10, facecolor='b', range=(0, 1), align='mid')
    plt.ylabel('Count in Bins')
    plt.xlabel('Bin Value')
    plt.title('Numpy Data 1 (Bin Size = 0.1)')
    plt.subplot(12, 2, 9)
    plt.hist(e, bins=10, facecolor='b', range=(0, 1), align='mid')
    plt.ylabel('Count in Bins')
    plt.xlabel('Bin Value')
    plt.title('Numpy Data 1 (Bin Size = 0.1)')
    plt.subplot(12, 2, 11)
    plt.hist(f, bins=10, facecolor='b', range=(0, 1), align='mid')
    plt.ylabel('Count in Bins')
    plt.xlabel('Bin Value')
    plt.title('Numpy Data 1 (Bin Size = 0.1)')
    
    # Generate for individual human runs, bin size 0.1
    plt.subplot(12, 2, 13)
    plt.hist(a, bins=40, facecolor='g', range=(0, 1), align='mid')
    plt.ylabel('Count in Bins')
    plt.xlabel('Bin Value')
    plt.title('Moss Data (Bin Size = 0.025)')
    plt.subplot(12, 2, 15)
    plt.hist(b, bins=40, facecolor='g', range=(0, 1), align='mid')
    plt.ylabel('Count in Bins')
    plt.xlabel('Bin Value')
    plt.title('Pratt Data (Bin Size = 0.025)')
    plt.subplot(12, 2, 17)
    plt.hist(c, bins=40, facecolor='g', range=(0, 1), align='mid')
    plt.ylabel('Count in Bins')
    plt.xlabel('Bin Value')
    plt.title('Strabala Data (Bin Size = 0.025)')
    
    # Generate for individual numpy runs, bin size 0.1
    plt.subplot(12, 2, 19)
    plt.hist(d, bins=40, facecolor='b', range=(0, 1), align='mid')
    plt.ylabel('Count in Bins')
    plt.xlabel('Bin Value')
    plt.title('Numpy Data 1 (Bin Size = 0.025)')
    plt.subplot(12, 2, 21)
    plt.hist(e, bins=40, facecolor='b', range=(0, 1), align='mid')
    plt.ylabel('Count in Bins')
    plt.xlabel('Bin Value')
    plt.title('Numpy Data 1 (Bin Size = 0.025)')
    plt.subplot(12, 2, 23)
    plt.hist(f, bins=40, facecolor='b', range=(0, 1), align='mid')
    plt.ylabel('Count in Bins')
    plt.xlabel('Bin Value')
    plt.title('Numpy Data 1 (Bin Size = 0.025)')
    
    # Sum up the individual runs
    humansum = np.concatenate((a, b, c))
    numpysum = np.concatenate((d, e, f))
    
    # Generate Sum runs for bin size 0.1
    plt.subplot(12, 2, 4)
    plt.hist(humansum, bins=10, facecolor='r', range=(0, 1), align='mid')
    plt.ylabel('Count in Bins')
    plt.xlabel('Bin Value')
    plt.title('Total Human Data (Bin Size = 0.1)')
    plt.subplot(12, 2, 10)
    plt.hist(numpysum, bins=10, facecolor='y', range=(0, 1), align='mid')
    plt.ylabel('Count in Bins')
    plt.xlabel('Bin Value')
    plt.title('Total Numpy Data (Bin Size = 0.1)')
    
    # Generate Sum runs for bin size 0.025
    plt.subplot(12, 2, 16)
    plt.hist(humansum, bins=40, facecolor='r', range=(0, 1), align='mid')
    plt.ylabel('Count in Bins')
    plt.xlabel('Bin Value')
    plt.title('Total Human Data (Bin Size = 0.025)')
    plt.subplot(12, 2, 22)
    plt.hist(numpysum, bins=40, facecolor='y', range=(0, 1), align='mid')
    plt.ylabel('Count in Bins')
    plt.xlabel('Bin Value')
    plt.title('Total Numpy Data (Bin Size = 0.025)')
    
    # Save the figure
    plt.savefig('../images/moss_project1Bii.png')
