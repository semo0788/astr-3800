# -*- coding: utf-8 -*-
"""
Created on Wed Sep 09 11:15:14 2015

@author: Sean
"""

import numpy as np
import saverestore

# Chi-Squared function: sum[i=1..n] (O_i - E_i)^2 / E_i
#   n is the number of bins, N is the total number of values
#   O_i is the observed value count in the ith bin
#   E_i is the expected value count in the ith bin, for this it should be N/n

def action():
    
    frequencyTest()
    
def frequencyTest():
    
    a = saverestore.restore('../data/moss_project1Ai.dat')[0]
    b = saverestore.restore('../data/pratt_project1Ai.dat')[0]
    c = saverestore.restore('../data/strabala_project1Ai.dat')[0]
    (d, e, f) = saverestore.restore('../data/moss_project1Aii.dat')
    
    humansum = np.concatenate((a, b, c))
    numpysum = np.concatenate((d, e, f))
    
    # 10 Bin Data
    (h10, e1) = np.histogram(humansum, bins=10, range=(0, 1))
    (n10, e1) = np.histogram(numpysum, bins=10, range=(0, 1))
    
    # 40 Bin Data
    (h40, e1) = np.histogram(humansum, bins=40, range=(0, 1))
    (n40, e1) = np.histogram(numpysum, bins=40, range=(0, 1))
    
    # 100 Bin Data
    (h100, e1) = np.histogram(humansum, bins=100, range=(0, 1))
    (n100, e1) = np.histogram(numpysum, bins=100, range=(0, 1))
    
    # Chi-squared solutions
    h1 = chiSquared(h10, 3000, 10)
    n1 = chiSquared(n10, 3000, 10)
    h2 = chiSquared(h40, 3000, 40)
    n2 = chiSquared(n40, 3000, 40)
    h3 = chiSquared(h100, 3000, 100)
    n3 = chiSquared(n100, 3000, 100)
    
    # Print solutions
    print ('Bin Count\t|\tHuman\t|\tNumpy')
    print (' 10 Bins\t|\t', h1, '\t|\t', n1)
    print (' 40 Bins\t|\t', h2, '\t|\t', n2)
    print (' 100 Bins\t|\t', h3, '\t|\t', n3)


# Calculates and returns the chi-squared value for the histogram data
#   O = histogram data, N = total values, n = number of bins
def chiSquared(O, N, n):
    
    Ei = float(N) / n
    
    total = 0
    for val in O:
        
        total += (((val - Ei)**2) / Ei)
        
    return total