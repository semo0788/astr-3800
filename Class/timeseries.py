import numpy as np
import matplotlib.pyplot as plt


def generateTime(deltat):
    
    diff = 1.0 - deltat
    step = diff / 128.0
    return np.arange(0, diff, step, dtype='float64')
    
    
def generateSignal(amp, freq, phase, time):
    
    return amp * np.sin(2 * np.pi * freq * time + phase)


def run():
    
    # Frequencies: 1, 5, 10, 16, 16.4, 32, 32.4, 64, 132, 144, 150, 192
    freq = [ 1, 5, 10, 16, 16.4, 32, 32.4, 64, 128, 132, 144, 150, 192 ]    
    
    time = generateTime(0.0)
    
    for i, f in enumerate(freq):
        
        signal = generateSignal(1.0, f, 0.0, time)
    
        plt.figure(i, figsize=(10, 8))
        plt.title('Freq: %.1f' % (f))
        plt.plot(time, signal)
        plt.scatter(time, signal, s=9)
        plt.xlim((0, 1))
        plt.ylim((-1, 1))
        plt.show()
        
        
def overplotSimilar():
    
    # Frequencies: 1, 5, 10, 16, 16.4, 32, 32.4, 64, 132, 144, 150, 192
    freq = [ 0, 64, 128, 1, 65, 129, 2, 66, 130, 8, 72, 136, 16, 80, 144 ]    
    
    time = generateTime(0.0)
    
    for i, f in enumerate(freq):
        
        signal = generateSignal(1.0, f, 0.0, time)
    
        plt.figure(i, figsize=(10, 8))
        plt.title('Freq: %.1f' % (f))
        plt.plot(time, signal)
        plt.scatter(time, signal, s=9)
        plt.xlim((0, 1))
        plt.ylim((-1, 1))
        plt.show()
        

def plotSum():
    
    time = generateTime(0.0)
    
    sin1 = np.sin(2 * np.pi * 3 * time)
    sin2 = np.sin(2 * np.pi * 7 * time)
    sin3 = np.sin(2 * np.pi * 129 * time)
    
    plt.figure(1, figsize=(10, 8))
    plt.plot(time, sin1 + sin2)
    plt.scatter(time, sin1 + sin2, s=9)
    plt.plot(time, sin3, c='r')
    plt.scatter(time, sin3, s=9, c='r')
    plt.xlim((0, 1))
    plt.ylim((-2.25, 2.25))
    plt.show()
    