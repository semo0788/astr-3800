# -*- coding: utf-8 -*-
"""
Created on Mon Nov 09 15:26:36 2015

@author: Sean
"""

import numpy as np
import matplotlib.pyplot as plt

def run():
    plt.ion()
    plt.close()
    plt.figure(0, figsize=(7.5, 9))
    
    pi = 4.0*np.arctan(1e0)
    
    nt = np.float64(128)
    time = np.arange(0, nt, dtype=np.float64)
    time = time/nt
    freq = 5.0
    phi = 0.0
    signal = np.sin(2e0 * pi * freq * time + phi)
    
    tsize = np.size(time)
    autocor = np.zeros(tsize)
    for shift in range(0, tsize):
        autocor[shift] = np.sum(signal * np.roll(signal, shift))
        
    plt.subplot(2, 1, 1)
    plt.plot(time, signal)
    plt.subplot(2, 1, 2)
    plt.plot(time, autocor)