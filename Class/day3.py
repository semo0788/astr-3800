# -*- coding: utf-8 -*-
"""
Created on Wed Sep  2 15:02:29 2015

@author: Sean
"""

# Chi Squared Rules
#	1. 80% of the bins should have Ek >= 0
#	2. Distribution should have enough bins to resolve the shape of the function
#	3. 
# DoF (Degrees of Freedom) : Number of constraining parameters
# Target Chi Squared Values
#	DoF - The expected chi squared is the degrees of freedom
#	0 - Possible value, but very suspect becasue it means observations match predicitons exactly
#	1 - Reduced Chi Squared should be equal to 1, this means there is no reason to suspect 
#	>1 - Unlikely that the data is drawn from the expected values