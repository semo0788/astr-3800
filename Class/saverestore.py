# -*- coding: utf-8 -*-
"""
Created on Wed Sep  2 16:22:10 2015

@author: Sean
"""

import numpy as np

def save(filename, names, data):	
	"""
	:param str filename: Name of the file to save the variables in.
	:param str tuple names: Names of the variables to save.
	:param np.array tuple data: The variables to save.
	"""
	
	with open(filename, 'wb') as outfile:
		
		for i, name in enumerate(names):
			
			outfile.write(name + '\n')
			value = data[i]
			shape = value.shape
			dtype = str(value.dtype)
			shape = ','.join(np.array(shape, dtype=str))
			outfile.write(shape + '\n')
			outfile.write(dtype + '\n')
			
			var_str = value.flatten().tobytes()
			outfile.write(var_str + '\n\n')
			
def restore(filename):
	"""
	:param str filename: Name of the file to save the variables in.
	"""
	
	with open(filename, 'rb') as infile:
		
		print ('Restoring variables: \n')
		ret_list = []
		while True:
			
			var_name = infile.readline()
			if var_name == '':
				break
			print('Read in variable: "%s"\n' % var_name)
			
			var_shape = infile.readline()
			var_shape = var_shape.split(',')
			var_shape = np.array(var_shape, dtype=int)
			var_dtype = infile.readline()
			
			var_value = ''
			line = ''
			while line != '\n':
				
				var_value += line
				line = infile.readline()
				
			var_value = np.fromstring(var_value[:-1], dtype=var_dtype)
			var_value = var_value.reshape(var_shape)
			ret_list.append(var_value)
			
		return ret_list