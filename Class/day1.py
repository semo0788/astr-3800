# -*- coding: utf-8 -*-
"""
Created on Wed Aug 26 16:25:14 2015

@author: Sean
"""

def part3a():
			
	for i in range(4, 101):
		for j in range(2, int(i / 2)):
			
			if i % j == 0:
				break
			if j == int(i / 2):
				print(i)

def part3b():
	pass

def part3c():
	pass

def part4a():
	pass

def part4b():
	pass

def testPi():
	
	import random
	import math
	total = 0
	total_in = 0
	for i in range(0, 100000000):
		
		if i % 1000000 == 0:
			print ('Percent: %f' % (i * 100 / 100000000.0))
			
		(x, y) = (random.random(), random.random())
		total += 1
		if math.sqrt(abs(x - 0.5)**2 + abs(y - 0.5)**2) <= 0.5:
			total_in += 1
		
	print ('The value of pi is approx. %f' % ((float(total_in) / total) * 4))