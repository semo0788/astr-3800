# -*- coding: utf-8 -*-
"""
Created on Wed Sep 09 15:30:10 2015

@author: Sean
"""

# Properties of distributions
#   Mean: (Average) written as x-bar (Sum of all elements / total number of elements)
#   Deviation: The difference between the mean and median
#   Average Deviation: (α)

from astropy.io import fits as pyfits
import numpy as np
import matplotlib.pyplot as plt

def display():
    
    fits = pyfits.open('data/p2.fits')
    scidata = fits[0].data
    
    