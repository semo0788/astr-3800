# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 15:32:13 2015

@author: Sean
"""

# Principal of maximum likelyhood

# Given n observations/measurements, x1, ..., xn, the best esitamate for the
# distribution parameters, mu and sigma, are those which make the observed values
# most likely

# This makes mu ~= sum(xi) / N = x-bar
# This makes sigma ~= root mean square (RMS), (use N - 1 version)

