# -*- coding: utf-8 -*-
"""
Created on Mon Aug 31 15:44:34 2015

@author: Sean
"""

def part2a(verbose=True):
	
	accum = []
	for i in range(1, 1000001):
		accum.append(i)
	
	if verbose:	
		print (len(accum))
	
def part2b(verbose=True):
	
	import numpy as np
	
	accum = np.array([], dtype=int)
	accum = np.append(accum, range(1, 1000001))
	
	if verbose:	
		print (len(accum))
	
def part2c(verbose=True):
	
	import numpy as np
	
	accum = np.empty(1000000, dtype=int)
	for i in range(0, 1000000):
		accum[i] = i + 1
		
	if verbose:
		print (len(accum))
	
def timePart2():
	
	import time
	
	start = time.clock()
	part2a(False)
	print ('Part 2a: %f seconds' % (time.clock() - start))
	
	start = time.clock()
	part2b(False)
	print ('Part 2b: %f seconds' % (time.clock() - start))
	
	start = time.clock()
	part2c(False)
	print ('Part 2c: %f seconds' % (time.clock() - start))
	
def part3a():
	
	x = [1, 2, 3, 4, 5, 6]
	y = x[2:5]
	y[0] = 999
	print (y)
	print (x)
	
def part3b():
	
	import numpy as np	
	
	x = np.array([1, 2, 3, 4, 5, 6])
	y = x[2:5]
	y[0] = 999
	print (y)
	print (x)