# -*- coding: utf-8 -*-
"""
Created on Wed Oct 07 15:33:40 2015

@author: Sean
"""

# Chi squared test for matching data to the best fit line

#   chi_squared = sum(i, ((y_i - A*x_i - B) ^ 2) / sigma_y)

#   sigma_y = sqrt((1/(N-2)) * sum(i, (y_i - A*x_i - B) ^ 2))