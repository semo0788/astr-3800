# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 16:16:46 2015

@author: Sean
"""

import numpy as np
import matplotlib.pyplot as plt


def run():
    
    print 'Loading Data From File'
    times, transits = loadData()
    print 'Running Part Ia (Cross-Corellation in Physical Space)'
    psts = partia(times, transits)
    print 'Running Part II (Plot Shifted Transits)'
    partii(times, transits, psts)


def loadData():
    """
    Loads the data from the file and finds the transit limits again.
    Returns the different transit slices as lists.
    """
    # Load the data from the file
    date, flux, dflux = np.genfromtxt('../data/wasp_4b.tsv', skiprows=36, delimiter=';').T
    
    # Extract the time series limits
    limits = []
    lastLimit = 0
    for i in range(1, len(date)):
        # Crude but effective way of detecting large time jumps
        if (date[i] - date[i - 1]) >= 0.5 or i == (len(date) - 1):
            limits.append((lastLimit, i))
            lastLimit = i
    
    # Extract the transits            
    times = [ date[l[0]:l[1]] for l in limits ]
    transits = [ flux[l[0]:l[1]] for l in limits ]
    
    # Adjust each one to start at zero
    for i in range(len(limits)):
        times[i] = times[i] - times[i][0]        
        
    return times, transits
    

def partia(times, transits):
    """
    Performs a cross corellation between the transits in physical space.
    Returns an array of the maximum time shift for each transit.
    """
    # Resultant array
    timeshifts = np.zeros(len(times), dtype=int)
    
    # Interpolate the other transits to the second transit's time grid
    interp = np.array([ np.interp(times[1], times[i], transits[i]) for i in range(len(times)) ])
            
    # Assign the reference transits
    refflux = interp[1]
    
    # Define a function to cross correllate with a shift
    def crossCorrellate(sdata, shift):
        
        # Roll the data by the given shift
        newdata = np.roll(sdata, shift)
        
        # Calculate the correlation factor
        results = refflux * newdata
        return np.sum(results)
    
    # Loop through each transit and find the maximum shift
    timelength = len(interp[1])
    for i in range(len(interp)):
        # Skip the reference transit, we know it is zero
        if i == 1:
            continue
        
        # Loop through the shifts and find the maximum correllated value
        maxval = 0
        maxshift = 0
        for j in range(timelength):
            corr = crossCorrellate(interp[i], -j)
            if corr > maxval:
                maxval = corr
                maxshift = -j
        
        # Save the maximum shift
        timeshifts[i] = maxshift
    
    print '\tCalculated timeshifts in physical space:', timeshifts
    return timeshifts
        
        
def partii(times, transits, psts):
    """
    Plots the transits shifted by their cross-correllated time shifts.
    """
    # Prepare the figure
    plt.figure(0, figsize=(10, 10))
    
    # Create the colors
    colors = [ 'blue', 'red', 'orange', 'purple', 'cyan', 'green' ]
    
    # Actually perform the time shifts
    deltatime = times[1][1] - times[1][0]
    for i in range(len(times)):
        times[i] = times[i] + (float(psts[i]) * deltatime)
    
    # Plot the transits
    plt.title('Overplotted Shifted Transits')
    plt.xlabel('Adjusted Time (Jul Date)')
    plt.ylabel('Relative Flux')
    for i in range(len(times)):

        # Pull out the data        
        t = times[i]
        f = transits[i]
        
        # Plot away
        plt.scatter(t, f, s=10, c=colors[i], label='Transit %d' % (i + 1))
    
    # Put in the legend
    plt.legend(loc='lower right')
    
    # Save the image
    plt.tight_layout()
    plt.savefig('../images/moss_project6Bii.png')