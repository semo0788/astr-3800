# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 16:32:35 2015

@author: Sean
"""

import numpy as np


def run():
    
    print 'Loading Data From File'
    time, flux, dflux = loadData()
    print 'Running Part I (Calculating Flux Depth Ratio)'
    fratio = parti(time, flux)
    print 'Running Part II (Calculating Flux Depth Ratio Uncertainty)'
    dfratio = partii(time, flux, dflux, fratio)
    print 'Running Part III (Calculating Planetary Radius and Uncertainty)'
    prad, dprad = partiii(fratio, dfratio)
    print 'Running Part IV (Calculating Planetary Density and Uncertainty)'
    partiv(prad, dprad)


def loadData():
    """
    Loads the data from the file. Returns the times, fluxes, and flux uncertainties.
    """
    # Load the data from the file
    date, flux, dflux = np.genfromtxt('../data/wasp_4b.tsv', skiprows=36, delimiter=';').T
    return date, flux, dflux
   

def parti(time, flux):
    """
    Calculates the ratio between transit and non-transit flux.
    Returns this ratio.
    """
    # Extract data for each domain
    ntindex = np.where(flux > .997)
    tindex = np.where(flux < .98)
    ntflux = flux[ntindex]
    tflux = flux[tindex]
    
    # Extract means
    ntmean = np.mean(ntflux)
    tmean = np.mean(tflux)
    
    # Report
    print '\tCalculated Flux Depth Ratio = %f.' % (tmean / ntmean)
    return (tmean / ntmean)
    
    
def partii(time, flux, dflux, fratio):
    """
    Uses error propogation to calculate the uncertainty in depth ratio.
    Returns this error.
    """
    # Extract data for each domain
    ntindex = np.where(flux > .997)
    tindex = np.where(flux < .98)
    ntflux = flux[ntindex]
    tflux = flux[tindex]
    ntdflux = dflux[ntindex]
    tdflux = dflux[tindex]
    
    # Get lengths
    ntlen = len(ntflux)
    tlen = len(tflux)
    
    # Sum the uncertainty ratios for each value
    ntsum = 0
    tsum = 0
    for i in range(ntlen):
        ratio = (ntdflux[i] / ntflux[i])**2
        ntsum += ratio
    for i in range(tlen):
        ratio = (tdflux[i] / tflux[i])**2
        tsum += ratio
        
    # Add them together and perform necessary operations
    totalsum = ntsum + tsum
    totalsum = np.sqrt(totalsum)
    totalsum *= fratio
    
    # Report and return
    print '\tCalculated uncertainty in ratio is +/- %f.' % (totalsum)
    return totalsum
    
    
def partiii(fratio, dfratio):
    """
    Calculates the radius of the planet WASP-4b, and the uncertainty in this value.
    This uses the equation (1 - FluxRatio) = (RadiusPlanet / RadiusStar) ** 2.
    This equation solves to RadiusPlanet = RadiusStar * sqrt(1 - FluxRatio).
    Returns the radius and uncertainty.
    """
    # Set up constants (in solar radii)
    STAR_RAD = 0.9
    STAR_DRAD = 0.02
    
    # Conversion constants (solar radii to km and solar radii to jupiter radii)
    SRAD_TO_KM = 695500.
    SRAD_TO_JRAD = (1. / 0.10045)
    
    # Calculate the solved planet radius, and report
    rPlanet = STAR_RAD * np.sqrt(1 - fratio)
    print '\tCalculated planetary radius = %f Jupiter Radii, %f Kilometers' % \
        (rPlanet * SRAD_TO_JRAD, rPlanet * SRAD_TO_KM)
    
    # Calculate the uncertainty in planet radius
    # Allow q(x, y) = x * sqrt(1 - y), use uncertainty for function of multiple variables
    dx = np.sqrt(1 - fratio)
    dx *= STAR_DRAD
    dy = -STAR_RAD / (2 * np.sqrt(1 - fratio))
    dy *= dfratio
    uncer = rPlanet * np.sqrt(dx**2 + dy**2) / 2
    
    # Report and return
    print '\tCalculated uncertainty in planetary radius = %f Jupiter Radii, %f Kilometers' % \
        (uncer * SRAD_TO_JRAD, uncer * SRAD_TO_KM)
    return rPlanet, uncer
    
    
def partiv(prad, dprad):
    """
    Calculates the density and uncertainty in density for WASP-4b.
    """
    # Constants
    PLANET_MASS = 2.348e30 # grams
    PLANET_DMASS = 1.139e29 # grams
    
    # Conversions
    SRAD_TO_KM = 6.96e5 # km
    KM3_TO_CM3 = 1e15 # km^3 to cm^3
    
    # Calculate volume (and uncertainty)
    kmprad = prad * SRAD_TO_KM
    kmdprad = dprad * SRAD_TO_KM
    volume = (4. * 3.1415926 / 3.) * (kmprad**3)
    dvolume = volume * 3 * kmdprad / kmprad / 2
    
    # Report
    print '\tCalculated volume is (%f +/- %f) Jupiter Volumes.' % (volume / 1.4313e15, dvolume / 1.4313e15)
    
    # Calculate density (and uncertainty)
    density = PLANET_MASS / (volume * KM3_TO_CM3)
    dx = (PLANET_DMASS / PLANET_MASS)**2
    dy = (dvolume / volume)**2
    ddensity = density * np.sqrt(dx + dy) / 2
    
    # Report
    print '\tCalculated density is (%f +/- %f) g/cm^3.' % (density, ddensity)