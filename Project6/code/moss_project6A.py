# -*- coding: utf-8 -*-
"""
Created on Mon Nov 09 15:01:35 2015

@author: Sean
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as pltgs


def run():
    
    print 'Running Part I (Loading Data From File)'
    date, flux, dflux = parti()
    print 'Running Part II (Plotting Time Series and Transits)'
    limits = partii(date, flux)
    print 'Running Part III (Extracting Transit Data)'
    transits = partiii(date, flux, limits)
    print 'Running Part IV (Overplotting Transits)'
    partiv(transits)


def parti():
    """
    Loading in the data from the file. Skip the first 36 lines.
    """
    date, flux, dflux = np.genfromtxt('../data/wasp_4b.tsv', skiprows=36, delimiter=';').T
    return date, flux, dflux
    
    
def partii(date, flux):
    """
    Plotting the full time series, and then the separate transit events.
    Returns the detected edges of the transits.
    """
    # Prepare the figure
    plt.figure(0, figsize=(10, 15))
    
    # Set up the subplot locations on the grid
    gs = pltgs.GridSpec(4, 2)
    axfull = gs[0, :]
    axt1 = gs[1, 0]
    axt2 = gs[1, 1]
    axt3 = gs[2, 0]
    axt4 = gs[2, 1]
    axt5 = gs[3, 0]
    axt6 = gs[3, 1]
    axtransits = [ axt1, axt2, axt3, axt4, axt5, axt6 ]
    
    # Extract the time series limits
    limits = []
    lastLimit = 0
    for i in range(1, len(date)):
        # Crude but effective way of detecting large time jumps
        if (date[i] - date[i - 1]) >= 0.5 or i == (len(date) - 1):
            limits.append((lastLimit, i))
            lastLimit = i
    
    # Plot the full time series
    plt.subplot(axfull)
    plt.title('Full Time Series')
    plt.xlabel('Time (Jul Date)')
    plt.ylabel('Relative Flux')
    plt.scatter(date, flux)
    
    # Plot the different transits
    for i, loc in enumerate(axtransits):
        plt.subplot(loc)
        plt.title('Transit %d' % (i + 1))
        plt.xlabel('Time (Jul Date)')
        plt.ylabel('Relative Flux')
        lims = limits[i]
        plt.scatter(date[lims[0]:lims[1]], flux[lims[0]:lims[1]])
    
    # Save the image
    plt.tight_layout()
    plt.savefig('../images/moss_project6Aii.png')
    
    # Return limits
    return limits
    

def partiii(date, flux, limits):
    """
    Extracts the transits from the data and resets their times.
    """
    # Resulting times array
    transits = []    
    
    # Pull out limits found in part ii
    for lims in limits:
        
        # Extract the datas
        trange = date[lims[0]:lims[1]]
        frange = flux[lims[0]:lims[1]]
        
        # Reset the times
        trange = trange - trange[0]
        
        # Add to the resultant array
        transits.append((trange, frange))
    
    # Return the transit data
    return transits


def partiv(transits):
    """
    Overplots the transit data on top of each other.
    """
    # Prepare the figure
    plt.figure(1, figsize=(10, 10))
    
    # Create the colors
    colors = [ 'blue', 'red', 'orange', 'purple', 'cyan', 'green' ]
    
    # Plot the transits
    plt.title('Overlapped Transits')
    plt.xlabel('Adjusted Time (Jul Date)')
    plt.ylabel('Relative Flux')
    for i, transit in enumerate(transits):

        # Pull out the data        
        times = transit[0]
        fluxes = transit[1]
        
        # Plot away
        plt.scatter(times, fluxes, s=10, c=colors[i], label='Transit %d' % (i + 1))
    
    # Put in the legend
    plt.legend(loc='lower right')
    
    # Save the image
    plt.tight_layout()
    plt.savefig('../images/moss_project6Aiv.png')